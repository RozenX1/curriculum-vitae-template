#!/usr/bin/make -f
# -*- mode:makefile -*-

clean:
	rm -f *.pdf *.toc *.fdb_latexmk *.gz *.aux *.log *.lot *.lof *.blg *.xml *-blx.bib *.bbl *.out
